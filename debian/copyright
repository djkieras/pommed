Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pommed
Source: http://alioth.debian.org/projects/pommed

Files: *
Copyright: Copyright (C) 2006-2008 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: client-common/*
Copyright: 2006-2007, 2009 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: wmpomme/*
Copyright: 2006-2009 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: gpomme/conffile.c
Copyright: 2007 daniel g. siegel <dgsiegel@gmail.com>
	2007 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: gpomme/theme.c
Copyright: 2006-2007 Julien BLACHE <jb@jblache.org>
	2006 Soeren SONNENBURG <debian@nn7.de>
License: GPL-2

Files: gpomme/gpomme.c
Copyright: 2006-2009 Julien BLACHE <jb@jblache.org>
	2007 daniel g. siegel <dgsiegel@gmail.com>
	2006, 2008 Soeren SONNENBURG <debian@nn7.de>
License: GPL-2

Files: pommed/power.c pommed/pommed.c pommed/kbd_auto.c pommed/conffile.c
	pommed/pmac/pmu.c
Copyright: 2006-2008 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: pommed/pmac/ofapi/of_standard.c
Copyright: 2006 Alastair Poole. netstar@gatheringofgray.com
License: GPL-2+

Files: pommed/pmac/ofapi/of_internals.c
Copyright: 2006 Alastair Poole. netstar@gatheringofgray.com
License: GPL-2+

Files: pommed/pmac/ofapi/of_externals.c pommed/pmac/ofapi/of_internals.h
	pommed/pmac/ofapi/of_api.h pommed/pmac/ofapi/of_standard.h
	pommed/pmac/ofapi/of_externals.h
Copyright: 2006 Alastair Poole. netstar@gatheringofgray.com)
License: GPL-2+

Files: pommed/pmac/kbd_backlight.c pommed/pmac/ambient.c
	pommed/mactel/gma950_backlight.c
Copyright: 2006 Yves-Alexis Perez <corsac@corsac.net>
	2006-2008 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files:./pommed/mactel/nv8600mgt_backlight.c
Copyright: 2006 Felipe Alfaro Solana <felipe_alfaro @linuxmail.org>
	2006 Nicolas Boichat <nicolas @boichat.ch>
	2007-2008 Julien BLACHE <jb@jblache.org>
License: GPL-2+

Files: pommed/mactel/acpi.c pommed/mactel/kbd_backlight.c 
Copyright: 2006-2008 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: pommed/mactel/x1600_backlight.c
Copyright: 2006-2007 Julien BLACHE <jb@jblache.org> / 2006 Nicolas Boichat <nicolas@boichat.ch>
License: GPL-2+

Files: pommed/mactel/ambient.c pommed/dbus.c
Copyright: 2006-2007 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: ./pommed/evdev.c
Copyright: 2006-2009 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: pommed/beep.c
Copyright: 2006-2008 Julien BLACHE <jb@jblache.org> / 2006 Soeren SONNENBURG <debian@nn7.de>
License: GPL-2

Files: pommed/audio.c
Copyright: 2006-2007 Julien BLACHE <jb@jblache.org> / 2006 Romain Beauxis <toots@rastageeks.org>
License: GPL-2

Files: pommed/cd_eject.c
Copyright: 2006-2007 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: pommed/sysfs_backlight.c
Copyright: 2006 Yves-Alexis Perez <corsac@corsac.net> / 2006-2008,2010 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: pommed/video.c
Copyright: 2007, 2009 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: pommed/evloop.c
Copyright: 2006-2008 Julien BLACHE <jb@jblache.org>
License: GPL-2

Files: debian/*
Copyright: (C) 2006-2008, Julien BLACHE <jblache@debian.org>
           (C) 2013, Nobuhiro Iwamatsu <iwamatsu@debian.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; either version 2 of the License.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; either version 2 of the License, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'

